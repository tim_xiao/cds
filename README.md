# The Valuation of Credit Default Swap with Counterparty Risk and Collateralization

There are two primary types of models that attempt to describe default processes in the literature: structural models and reduced-form (or intensity) models. Many practitioners in the credit trading arena have tended to gravitate toward the reduced-from models given their mathematical tractability. 

Central to the reduced-form models is the assumption that multiple defaults are independent conditional on the state of the economy. In reality, however, the default of one party might affect the default probabilities of other parties. Collin-Dufresne et al. (2003) and Zhang and Jorion (2007) find that a major credit event at one firm is associated with significant increases in the credit spreads of other firms. Giesecke (2004), Das et al. (2006), and Lando and Nielsen (2010) find that a defaulting firm can weaken the firms in its network of business links. These findings have important implications for the management of credit risk portfolios, where default relationships need to be explicitly modeled.

The main drawback of the conditionally independent assumption or the reduced-form models is that the range of default correlations that can be achieved is typically too low when compared with empirical default correlations (see Das et al. (2007)). The responses to correct this weakness can be generally classified into two categories: endogenous default relationship approaches and exogenous default relationship approaches. 

The endogenous approaches include the contagion (or infectious) models and frailty models. The frailty models (see Duffie et al. (2009), Koopman et al. (2011), etc) describe default clustering based on some unobservable explanatory variables. In variations of contagion or infectious type models (see Davis and Lo (2001), Jarrow and Yu (2001), etc.), the assumption of conditional independence is relaxed and default intensities are made to depend on default events of other entities. Contagion and frailty models fill an important gap but at the cost of analytic tractability. They can be especially difficult to implement for large portfolios.

The exogenous approaches (see Li (2000), Laurent and Gregory (2005), Hull and White (2004), Brigo et al. (2011), etc) attempt to link marginal default probability distributions to the joint default probability distribution through some external functions. Due to their simplicity in use, practitioners lean toward the exogenous ones.

Given a default model, one can value a risky derivative contract and compute credit value adjustment (CVA) that is a relatively new area of financial derivative modeling and trading. CVA is the expected loss arising from the default of a counterparty (see Brigo and Capponi (2008), Lipton and Sepp (2009), Pykhtin and Zhu (2006), Gregory (2009), Bielecki et al (2013) and Crepey (2015), Xiao (2015), Xiao (2017), etc.)

Collateralization as one of the primary credit risk mitigation techniques becomes increasingly important and widespread in derivatives transactions. According the ISDA (2013), 73.7% of all OTC derivatives trades (cleared ad non-cleared) are subject to collateral agreements. For large firms, the figure is 80.7%. On an asset class basis, 83.0% of all CDS transactions and 79.2% of all fixed income transactions are collateralized. For large firms, the figures are 96.3% and 89.4%, respectively. Previous studies on collateralization include Johannes and Sundaresan (2007), Fuijii and Takahahsi (2012), Piterbarg (2010), Bielecki, et al (2013) and Hull and White (2014), etc.

This paper presents a new framework for valuing defaultable financial contracts with or without collateral arrangements. The framework characterizes default dependencies exogenously, and models collateral processes directly based on the fundamental principals of collateral agreements. For brevity we focus on CDS contracts, but many of the points we make are equally applicable to other derivatives. CDS has trilateral credit risk, where three parties – buyer, seller and reference entity – are defaultable.

In general, a CDS contract is used to transfer the credit risk of a reference entity from one party to another. The risk circularity that transfers one type of risk (reference credit risk) into another (counterparty credit risk) within the CDS market is a concern for financial stability. Some people claim that the CDS market has increased financial contagion or even propose an outright ban on these instruments.

The standard CDS pricing model in the market assumes that there is no counterparty risk. Although this oversimplified model may be accepted in normal market conditions, its reliability in times of distress has recently been questioned. In fact, counterparty risk has become one of the most dangerous threats to the CDS market. For some time now it has been realized that, in order to value a CDS properly, counterparty effects have to be taken into account (see ECB (2009)).

We bring the concept of comvariance into the area of credit risk modeling to capture the statistical relationship among three or more random variables. Comvariance was first introduced to economics by Deardorff (1982), who used this measurement to correlate three factors in international trading. Furthermore, we define a new statistics, comrelation, as a scaled version of comvariance. Accounting for default correlations and comrelations becomes important in determining CDS premia, especially during the credit crisis. Our analysis shows that the effect of default dependencies on a CDS premium from large to small accordingly is i) the default correlation between the protection seller and the reference entity, ii) the default comrelation, iii) the default correlation between the protection buyer and the reference entity, and iv) the default correlation between the protection buyer and the protection seller. In particular, we find that the default comvariance/comrelation has substantial effects on the asset pricing and risk management, which have never been documented.

There is a significant increase in the use of collateral for CDS after the recent financial crises. Many people believe that, if a CDS is fully collateralized, there is no risk of failure to pay. Collateral posting regimes are originally designed and utilized for bilateral risk products, e.g., interest rate swap (IRS), but there are many reasons to be concerned about the success of collateral posting in offsetting the risk of CDS contracts. First, the value of CDS contracts tends to move very suddenly with big jumps, whereas the price movements of IRS contracts are far smoother and less volatile than CDS prices. Second, CDS spreads can widen very rapidly. Third, CDS contracts have many more risk factors than IRS contracts. In fact, our model shows that full collateralization cannot eliminate counterparty risk completely for a CDS contract.

This article presents a new valuation framework for pricing financial instruments subject to credit risk. In particular, we focus on modeling default relationships. 

To capture the default relationships among more than two defaultable entities, we introduce a new statistic: comrelation, an analogue to correlation for multiple variables, to exploit any multivariate statistical relationship. Our research shows that accounting for default correlations and comrelations becomes important, especially under market stress. The existing valuation models in the credit derivatives market, which take into account only pair-wise default correlations, may underestimate credit risk and may be inappropriate.

We study the sensitivity of the price of a defaultable instrument to changes in the joint credit quality of the parties. For instance, our analysis shows that the effect of default dependence on CDS premia from large to small is the correlation between the protection seller and the reference entity, the comrelation, the correlation between the protection buyer and the reference entity, and the correlation between the protection buyer and the protection seller.

The model shows that a fully collateralized CDS is not equivalent to a risk-free one. Therefore, we conclude that collateralization designed to mitigate counterparty risk works well for financial instruments subject to bilateral credit risk, but fails for ones subject to multilateral credit risk. 

References

Arora, Navneet, Priyank Gandhi, and Francis A. Longstaff (2009), “Counterparty credit risk and the credit default swap market,” Working paper, UCLA.

Brigo, D., A. Pallavicini, and R. Torresetti (2011), “Credit Models and the Crisis: default cluster dynamics and the Generalized Poisson Loss model,” Journal of Credit Risk, 6: 39-81.

Brigo, D., and Capponi, A., 2008, Bilateral counterparty risk valuation with stochastic dynamical models and application to Credit Default Swaps, Working paper.

Collin-Dufresne, P., R. Goldstein, and J. Helwege (2003), “Is credit event risk priced? Modeling contagion via the updating of beliefs,” Working paper, Haas School, University of California, Berkeley.

Das, S., D. Duffie, N. Kapadia, and L. Saita (2007), “Common failings: How corporate defaults are correlated,” Journal of Finance, 62: 93-117.

Das, S., L. Freed, G. Geng, N. Kapadia (2006), “Correlated default risk,” Journal of Fixed Income, 16: 7-32.

Davis, M., and V. Lo (2001), “Infectious defaults,” Quantitative Finance, 1: 382-387.

Bielecki, T., I. Cialenco and I. Iyigunler, (2013) “Collateralized CVA valuation with rating triggers and credit migrations,” International Journal of Theoretical and Applied Finance, 16 (2).

Crepey, S. (2015) “Bilateral counterparty risk under funding constraints – part II: CVA,” Mathematical Finance, 25(1), 23-50.

Deardorff, Alan V. (1982): “The general validity of the Heckscher-Ohlin Theorem,” American Economic Review, 72 (4): 683-694.

Duffie, D., A. Eckner, G. Horel, and L. Saita (2009), “Frailty correlated default,” Journal of Finance, 64: 2089-2123.

Duffie, D., and K. Singleton (1999), “Modeling term structure of defaultable bonds,” Review of Financial Studies, 12: 687-720.

ECB (2009), “Credit default swaps and counterparty risk,” European central bank.

FinPricing, 2018, Market data process, https://finpricing.com/lib/IrCurveIntroduction.html

Fuijii, M. and A. Takahahsi (2012), “Collateralized CDS and default dependence – Implications for the central clearing,” Journal of Credit Risk, 8(3): 97-113.

Giesecke, K. (2004), “Correlated default with incomplete information,” Journal of Banking and Finance, 28: 1521-1545.

Gregory, Jon, 2009, Being two-faced over counterparty credit risk, RISK, 22, 86-90.

Hull, J. and A. White (2004), “Valuation of a CDO and a nth to default CDS without Monte Carlo simulation,” Journal of Derivatives, 12: 8-23.

Hull, J. and A. White (2014), “Collateral and Credit Issues in Derivatives Pricing,” Journal of Credit Risk, 10 (3), 3-28.

ISDA (2013), “ISDA margin survey 2013.”

Jarrow, R., and S. Turnbull (1995), “Pricing derivatives on financial securities subject to credit risk,” Journal of Finance, 50: 53-85.

Jarrow, R., and F. Yu (2001), “Counterparty risk and the pricing of defaultable securities,” Journal of Finance, 56: 1765-99.

Johannes, M. and S. Sundaresan (2007), “The impact of collateralization on swap rates,” Journal of Finance, 62: 383-410.

J. P. Morgan (1999), “The J. P. Morgan guide to credit derivatives,” Risk Publications.

Koopman, S., A. Lucas, and B. Schwaab (2011), “Modeling frailty-correlated defaults using many macroeconomic covariates,” Journal of Econometrics, 162: 312-325.

Lando, D. and M. Nielsen (2010), “Correlation in corporate defaults: contagion or conditional independence?” Journal of Financial Intermediation, 19: 355-372.

Laurent, J. and J. Gregory (2005), “Basket default swaps, CDOs and factor copulas,” Journal of Risk, 7:  
03-122.

Li, D. (2000), “On default correlation: A copula function approach,” Journal of Fixed Income, 9: 43-54.

Lipton, A., and Sepp, A., 2009, Credit value adjustment for credit default swaps via the structural default model, Journal of Credit Risk, 5(2), 123-146.

Longstaff, F., and E. Schwartz (2001): “Valuing American options by simulation: a simple least-squares approach,” The Review of Financial Studies, 14 (1): 113-147.

Madan, D., and H. Unal (1998), “Pricing the risks of default,” Review of Derivatives Research, 2: 121-160.

Piterbarg, V. (2010), “Funding beyond discounting: collateral agreements and derivatives pricing,” Risk Magazine, 2010 (2): 97-102.

Pykhtin, Michael, and Steven Zhu, 2007, A guide to modeling counterparty credit risk, GARP Risk Review, July/August, 16-22.

Teugels, J. (1990), “Some representations of the multivariate Bernoulli and binomial distributions,” Journal of Multivariate Analysis, 32: 256-268.

Xiao, T., 2017, “A New Model for Pricing Collateralized OTC Derivatives.” Journal of Derivatives, 24(4), 8-20.

Xiao, T., 2015, “An Accurate Solution for Credit Valuation Adjustment (CVA) and Wrong Way Risk.” Journal of Fixed Income, 25(1), 84-95.

Zhang, G., and P. Jorion, (2007), “Good and bad credit contagion: Evidence from credit default swaps,” Journal of Financial Economics 84: 860–883.


